<?php

namespace Tests;

use UnitTester\Test;

class MockTest extends Test {

    public function test_mockedResponseShouldBeReturned()
    {
        $mockedClass = $this->mock(ClassToMock::class);

        $expectedResponse = 'mocked response';

        $mockedClass->expect('mockMe', [], $expectedResponse);

        $this->assertEquals($mockedClass->mockMe(), $expectedResponse);
    }

    public function test_mockedResponseShouldBeReturnedWhenArgumentsMatch()
    {
        $mockedClass = $this->mock(ClassToMock::class);

        $expectedResponse = 'mocked response';

        $mockedClass->expect('mockMeWithArguments', ['test'], $expectedResponse);

        $this->assertEquals($mockedClass->mockMeWithArguments('test'), $expectedResponse);
    }

    public function test_mockedResponseShouldNotBeReturnedWhenArgumentsDoNotMatch()
    {
        $mockedClass = $this->mock(ClassToMock::class);

        $expectedResponse = 'not mocked';

        $mockedClass->expect('mockMeWithArguments', ['test'], 'mocked response');

        $this->assertEquals($mockedClass->mockMeWithArguments('test2'), $expectedResponse);
    }

}

class ClassToMock {

    public function mockMe()
    {
        return 'not mocked';
    }

    public function mockMeWithArguments($arg1)
    {
        return 'not mocked';
    }

}