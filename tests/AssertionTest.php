<?php

namespace Tests;

use UnitTester\Test;
use UnitTester\Asserts\AssertException;

class AssertionTest extends Test {

    /**
     * @expectedToWarn
     */
    public function test_shouldThrowWarningSinceNoAssertionsAreRan()
    {
        // no assertions here
    }
    
    public function invalidTestName() 
    {
        throw new \Exception('test should have been skipped!');
    }

    /**
     * @expectedToFail
     */
    public function test_shouldFailAndStopExecutionWhenTriggeringTestToFail()
    {
        $this->fail();

        throw new \Exception('test should not reach this line when triggering test to fail');
    }

    /**
     * @expectedToFail
     */
    public function test_assertContainsShouldFailWhenValueIsNotInArray() 
    {
        $this->assertContains(['key'], 'notfound');
    }

    public function test_assertContainsShouldPassWhenValueIsInArray() 
    {
        $this->assertContains(['key'], 'key');
    }

    /**
     * @expectedToFail
     */
    public function test_assertContainsKeyShouldFailWhenKeyIsNotInArray() 
    {
        $this->assertContainsKey(['key' => 'value'], 'notfound');
    }

    public function test_assertContainsKeyShouldPassWhenKeyIsInArray() 
    {
        $this->assertContainsKey(['key' => 'value'], 'key');
    }

    /**
     * @expectedToFail
     */
    public function test_assertCountShouldFailWhenArrayCountDoesNotMatch() 
    {
        $this->assertCount(['key', 'value'], 3);
    }

    public function test_assertCountShouldPassWhenArrayCountMatches() 
    {
        $this->assertCount(['key', 'value'], 2);
    }

    /**
     * @expectedToFail
     */
    public function test_assertEqualsShouldFailWhenNotEqual() 
    {
        $this->assertEquals(3, 2);
    }

    public function test_assertEqualsShouldPassWhenEqual() 
    {
        $this->assertEquals(4, 4);
    }

    /**
     * @expectedToFail
     */
    public function test_assertNotEqualsShouldFailWhenEqual() 
    {
        $this->assertNotEquals(3, 3);
    }

    public function test_assertNotEqualsShouldPassWhenNotEqual() 
    {
        $this->assertNotEquals(4, 3);
    }

    /**
     * @expectedToFail
     */
    public function test_assertTrueShouldFailWhenFalse() 
    {
        $this->assertTrue(false);
    }

    public function test_assertTrueShouldPassWhenTrue() 
    {
        $this->assertTrue(true);
    }

    /**
     * @expectedToFail
     */
    public function test_assertFalseShouldFailWhenTrue() 
    {
        $this->assertFalse(true);
    }

    public function test_assertFalseShouldPassWhenFalse() 
    {
        $this->assertFalse(false);
    }

    /**
     * @expectedToFail
     */
    public function test_assertNullShouldFailWhenNotNull() 
    {
        $this->assertNull('not null');
    }

    public function test_assertNullShouldPassWhenNull() 
    {
        $this->assertNull(null);
    }

    /**
     * @expectedToFail
     */
    public function test_assertNotNullShouldFailWhenNull() 
    {
        $this->assertNotNull(null);
    }

    public function test_assertNotNullShouldPassWhenNotNull() 
    {
        $this->assertNotNull('not null');
    }

}