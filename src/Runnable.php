<?php

namespace UnitTester;

use UnitTester\Results\Result;

interface Runnable {

    public function execute(Result $result);

}