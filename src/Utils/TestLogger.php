<?php

namespace UnitTester\Utils;

class TestLogger {

    public static function log($message, $skipLineBreak = false)
    {
        echo $message;

        if (!$skipLineBreak)
        {
            echo PHP_EOL;
        }
    }

}