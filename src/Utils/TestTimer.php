<?php

namespace UnitTester\Utils;

class TestTimer {

    private $startTime;
    private $endTime;

    public function start()
    {
        $this->startTime = microtime(true);
        $this->endTime = null;
    }

    public function end()
    {
        $this->endTime = microtime(true);
    }

    public function reset()
    {
        $this->startTime = null;
        $this->endTime = null;
    }

    public function getResult()
    {
        if (is_null($this->startTime) || is_null($this->endTime))
        {
            return null;
        }

        return number_format(($this->endTime - $this->startTime), 8);
    }

}