<?php

namespace UnitTester\Results;

use UnitTester\Results\Result;
use UnitTester\Results\TestResult;

class TestSuiteResult extends Result {

    /** @var TestResult[] */
    private $testResults = [];

    /** @return TestResult[] */
    public function getTestResults()
    {
        return $this->testResults;
    }

    public function setTestResults(array $testResults)
    {
        $this->testResults = $testResults;
    }

}