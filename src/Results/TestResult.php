<?php

namespace UnitTester\Results;

use UnitTester\Results\Result;
use UnitTester\Results\TestCaseResult;

class TestResult extends Result {

    private $testCaseResults = [];
    private $testClassName;

    public function __construct($testClassName)
    {
        $this->testClassName = $testClassName;
    }

    public function getTestClassName()
    {
        return $this->testClassName;
    }

    public function getTestCaseResults()
    {
        return $this->testCaseResults;
    }

    public function addTestCaseResult(TestCaseResult $testCaseResult)
    {
        $this->testCaseResults[] = $testCaseResult;
    }

}