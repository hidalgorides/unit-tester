<?php

namespace UnitTester\Results;

abstract class Result {

    private $executionTime = 0;

    public function getExecutionTime()
    {
        return $this->executionTime;
    }

    public function setExecutionTime($executionTime)
    {
        $this->executionTime = $executionTime;
    }

}