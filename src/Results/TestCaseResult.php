<?php

namespace UnitTester\Results;

use UnitTester\Results\Result;
use UnitTester\TestCaseStatus;

class TestCaseResult extends Result {

    private $assertionsRan = 0;
    private $failureMessage;
    private $status = TestCaseStatus::PASSED;
    private $methodName;

    public function __construct($methodName)
    {
        $this->methodName = $methodName;
    }

    public function getMethodName()
    {
        return $this->methodName;
    }
    
    public function getAssertionsRan()
    {
        return $this->assertionsRan;
    }

    public function setAssertionsRan(int $assertionsRan)
    {
        $this->assertionsRan = $assertionsRan;
    }

    public function getFailureMessage()
    {
        return $this->failureMessage;
    }

    public function setFailureMessage($failureMessage)
    {
        $this->failureMessage = $failureMessage;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

}