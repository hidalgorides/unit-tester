<?php

namespace UnitTester;

use UnitTester\Test;
use UnitTester\Runnable;
use UnitTester\Results\Result;
use UnitTester\Utils\TestTimer;
use UnitTester\Results\TestCaseResult;
use UnitTester\Exceptions\AssertException;
use UnitTester\Exceptions\ForcedFailureException;

class TestCase implements Runnable {

    private $methodName;
    private $description;
    /** @var Test $test */
    private $test;
    private $expectedToFail = false;
    private $expectedToWarn = false;

    public function __construct(Test $test, $methodName, $description = null)
    {
        $this->test = $test;
        $this->methodName = $methodName;
        $this->description = $description;
    }

    public function getMethodName()
    {
        return $this->methodName;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setExpectedToFail($expectedToFail)
    {
        $this->expectedToFail = boolval($expectedToFail);
    }

    public function setExpectedToWarn($expectedToWarn)
    {
        $this->expectedToWarn = boolval($expectedToWarn);
    }

    /** @var TestCaseResult $testCaseResult */
    public function execute(Result $testCaseResult)
    {
        $errorMessage = null;
        $forceFailed = false;

        $this->test->setAssertionsRan(0);

        try {
            $this->test->beforeMethod();
            $this->test->{$this->methodName}();
        } catch (AssertException | ForcedFailureException $e) {
            if (!$this->expectedToFail)
            {
                $testCaseResult->setStatus(TestCaseStatus::FAILED);
                $testCaseResult->setFailureMessage($e->getMessage());
            }

            if ($e instanceOf ForcedFailureException)
            {
                $forceFailed = true;
            }
        } finally {
            $this->test->afterMethod();
        }

        if (!$forceFailed && !$this->expectedToWarn && count($this->test->getAssertions()) == 0)
        {
            $testCaseResult->setStatus(TestCaseStatus::WARNING);
        }

        $testCaseResult->setAssertionsRan($this->test->getAssertionsRan());
    }

}