<?php

namespace UnitTester;

use ReflectionClass;
use ReflectionMethod;
use UnitTester\Runnable;
use UnitTester\TestAction;
use UnitTester\Results\Result;
use UnitTester\Utils\TestLogger;
use UnitTester\Mocks\MockedClass;
use UnitTester\Results\TestResult;
use UnitTester\Results\TestCaseResult;
use UnitTester\Exceptions\ForcedFailureException;

abstract class Test extends AssertableTest implements Runnable {

    private static $TEST_METHOD_PREFIX = 'test_';

    private $className;

    private $testCaseFilters = [];

    public function __construct()
    {
        $this->className = get_class($this);
    }

    public function getClassName()
    {
        return $this->className;
    }

    public function setTestCaseFilters(array $testCaseFilters)
    {
        $this->testCaseFilters = $testCaseFilters;
    }

    public function beforeTest() {}
    public function afterTest() {}
    public function beforeMethod() {}
    public function afterMethod() {}

    /** @var TestResult $testResult */
    public function execute(Result $testResult) 
    {
        $testCasesToRun = $this->getTestCasesToRun();

        TestLogger::log('Running test cases in ' . $this->getClassName());

        if (count($testCasesToRun) == 0)
        {
            TestLogger::log('    No tests to run in ' . $this->getClassName());
            TestLogger::log('');
            return;
        }

        $this->beforeTest();

        $action = new TestAction();

        foreach ($testCasesToRun as $testCase)
        {
            TestLogger::log('    ' . $testCase->getMethodName(), true);

            $testCaseResult = $action->run($testCase, new TestCaseResult($testCase->getMethodName()));

            $testResult->addTestCaseResult($testCaseResult);

            $testCaseStatus = $testCaseResult->getStatus();

            TestLogger::log('    ' . $this->getTestStatusOutputColor($testCaseStatus) . $testCaseStatus . "\033[0m" . ' (' . $testCaseResult->getExecutionTime() . ' seconds)');
        }

        $this->afterTest();
    }

    protected function fail()
    {
        throw new ForcedFailureException('fail');
    }

    /** @return MockedClass */
    protected function mock($className)
    {
        return new MockedClass($className);
    }

    /**
     * @return TestCase[]
     */
    private function getTestCasesToRun()
    {
        $calledClass = get_called_class();
        $reflectionClass = new ReflectionClass($calledClass);
    
        $testCases = [];

        /** @var ReflectionMethod $method */
        foreach ($reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC) as $method)
        {
            if ($method->class == $reflectionClass->getName())
            {
                $methodName = $method->getName();

                if (strpos($methodName, self::$TEST_METHOD_PREFIX) === 0) 
                {
                    if (count($this->testCaseFilters) == 0 || in_array($methodName, $this->testCaseFilters))
                    {
                        $testCase = new TestCase($this, $methodName);
                        $testCaseDisabled = false;

                        $methodDocComment = $reflectionClass->getMethod($methodName)->getDocComment();

                        if ($methodDocComment)
                        {
                            preg_match_all('#@(.*?)\n#s', $methodDocComment, $methodAnnotations);

                            foreach ($methodAnnotations[1] as $methodAnnotation)
                            {
                                if ($methodAnnotation == 'expectedToFail')
                                {
                                    $testCase->setExpectedToFail(true);
                                }
                                else if ($methodAnnotation == 'expectedToWarn')
                                {
                                    $testCase->setExpectedToWarn(true);
                                }
                                else if ($methodAnnotation == 'disabled')
                                {
                                    $testCaseDisabled = true;
                                }
                            }
                        }

                        if (!$testCaseDisabled)
                        {
                            $testCases[] = $testCase;
                        }
                    }
                }
            }
        }

        return $testCases;
    }

    private function getTestStatusOutputColor($testCaseStatus)
    {
        $statusOutputColorStart = "\033[0m";

        switch ($testCaseStatus) {
            case TestCaseStatus::PASSED:
                $statusOutputColorStart = "\033[32m";
                break;
            case TestCaseStatus::FAILED:
                $statusOutputColorStart = "\033[31m";
                break;
            case TestCaseStatus::WARNING:
                $statusOutputColorStart = "\033[33m";
                break;
        }

        return $statusOutputColorStart;
    }
    
}