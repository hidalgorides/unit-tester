<?php

namespace UnitTester\Exceptions;

use Exception;

class ForcedFailureException extends Exception {}