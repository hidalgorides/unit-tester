<?php

namespace UnitTester;

use UnitTester\Test;
use UnitTester\Runnable;
use UnitTester\TestAction;
use UnitTester\Results\Result;
use UnitTester\Utils\TestTimer;
use UnitTester\Utils\TestLogger;
use UnitTester\Results\TestResult;
use UnitTester\Results\TestSuiteResult;

class TestSuite implements Runnable {

    private $name;

    /** @var Test[] */
    private $tests = [];

    public function __construct($name) 
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function addTest(Test $test)
    {
        $this->tests[] = $test;
    }

    /** @var TestSuiteResult $result */
    public function execute(Result $result)
    {
        TestLogger::log('Running test suite: ' . $this->getName());

        $testResults = [];

        $executionTimer = new TestTimer();
        $executionTimer->start();

        $action = new TestAction();

        foreach ($this->tests as $test)
        {
            $testResults[] = $action->run($test, new TestResult($test->getClassName()));
        }

        $executionTimer->end();

        $result->setTestResults($testResults);
    }

}