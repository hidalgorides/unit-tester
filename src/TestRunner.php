<?php

namespace UnitTester;

use UnitTester\TestSuite;
use UnitTester\TestAction;
use UnitTester\Utils\TestTimer;
use UnitTester\Utils\TestLogger;
use UnitTester\Results\TestResult;
use UnitTester\Results\TestCaseResult;
use UnitTester\Results\TestSuiteResult;

class TestRunner {

    /** @var TestSuite[] */
    private $suites = [];

    public function addSuite(TestSuite $suite)
    {
        $this->suites[] = $suite;
    }

    public function run()
    {
        TestLogger::log('Test runner starting...');

        $executionTimer = new TestTimer();
        $executionTimer->start();

        /** @var TestResult[] $testResults */
        $testSuiteResults = [];

        if (count($this->suites) == 0)
        {
            TestLogger::log('No suites registered to run');
        }
        else
        {
            $action = new TestAction();

            foreach ($this->suites as $suite)
            {
                $testSuiteResults[] = $action->run($suite, new TestSuiteResult());

                TestLogger::log('');
            }
        }

        $executionTimer->end();

        $numberOfTestsRan = 0;
        $numberOfTestCasesRan = 0;
        $numberOfAssertionsRan = 0;
        $numberOfFailedTestCases = 0;
        $numberOfPassedTestCases = 0;
        $numberOfWarningTestCases = 0;
        $totalTimeTaken = 0;

        foreach ($testSuiteResults as $testSuiteResult)
        {
            $testResults = $testSuiteResult->getTestResults();
            $numberOfTestsRan += count($testResults);

            /** @var TestResult $testResult */
            foreach ($testResults as $testResult)
            {
                $testCaseResults = $testResult->getTestCaseResults();
                $numberOfTestCasesRan += count($testCaseResults);

                /** @var TestCaseResult $testCaseResult */
                foreach ($testCaseResults as $testCaseResult)
                {
                    $numberOfAssertionsRan += $testCaseResult->getAssertionsRan();

                    if ($testCaseResult->getStatus() == TestCaseStatus::FAILED)
                    {
                        $numberOfFailedTestCases++;
                    } 
                    else if ($testCaseResult->getStatus() == TestCaseStatus::WARNING)
                    {
                        $numberOfWarningTestCases++;
                    }
                    else if ($testCaseResult->getStatus() == TestCaseStatus::PASSED)
                    {
                        $numberOfPassedTestCases++;
                    }
                }
            }

            $totalTimeTaken += $testSuiteResult->getExecutionTime();
        }
        
        TestLogger::log('Test runner completed!');
        TestLogger::log('');
        TestLogger::log('Total tests ran: ' . $numberOfTestsRan);
        TestLogger::log('Total test cases ran: ' . $numberOfTestCasesRan);
        
        TestLogger::log('   Passed: ' . $numberOfPassedTestCases);
        TestLogger::log('   Warning: ' . $numberOfWarningTestCases);
        TestLogger::log('   Failed: ' . $numberOfFailedTestCases);
        
        TestLogger::log('Total test assertions ran: ' . $numberOfAssertionsRan);
        TestLogger::log('Total time taken in seconds: ' . $executionTimer->getResult());
    }

}