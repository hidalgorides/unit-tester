<?php

namespace UnitTester;

class TestCaseStatus {

    const FAILED = 'FAILED';
    const PASSED = 'PASSED';
    const SKIPPED = 'SKIPPED';
    const WARNING = 'WARNING';
    
}