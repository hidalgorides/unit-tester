<?php

namespace UnitTester;

use UnitTester\Runnable;
use UnitTester\Results\Result;
use UnitTester\Utils\TestTimer;

class TestAction {

    /** @var TestTimer */
    private $executionTimer;

    public function __construct()
    {
        $this->executionTimer = new TestTimer();
    }

    /** @return Result */
    public function run(Runnable $runnable, Result $result)
    {
        $this->executionTimer->reset();
        $this->executionTimer->start();

        $runnable->execute($result);
        
        $this->executionTimer->end();
        
        $result->setExecutionTime($this->executionTimer->getResult());

        return $result;
    }

}