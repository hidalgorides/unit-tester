<?php

namespace UnitTester\Mocks;

use ReflectionClass;

class MockedClass {

    private $class;
    private $methods = [];
    private $expectations = [];

    public function __construct($className)
    {
        $reflectionClass = new ReflectionClass($className);
        $this->class = new $className();

        foreach ($reflectionClass->getMethods() as $method)
        {
            $this->methods[] = $method->getName();
        }
    }

    public function __call($methodName, $arguments)
    {
        if (in_array($methodName, $this->methods))
        {
            if ($this->isExpectedMethodCall($methodName, $arguments))
            {
                return $this->expectations[$methodName][$this->argumentsToKey($arguments)];
            }

            return $this->class->{$methodName}(...$arguments);
        }
    }

    public function expect($methodName, array $arguments = [], $returnValue)
    {
        $this->expectations[$methodName][$this->argumentsToKey($arguments)] = $returnValue;
    }

    private function isExpectedMethodCall($methodName, $arguments)
    {
        return (array_key_exists($methodName, $this->expectations) && array_key_exists($this->argumentsToKey($arguments), $this->expectations[$methodName]));
    }

    public function argumentsToKey(array $arguments)
    {
        return serialize($arguments);
    }

}