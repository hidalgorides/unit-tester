<?php

namespace UnitTester;

use UnitTester\Asserts\Assertable;
use UnitTester\Asserts\AssertNull;
use UnitTester\Asserts\AssertTrue;
use UnitTester\Asserts\AssertCount;
use UnitTester\Asserts\AssertFalse;
use UnitTester\Asserts\AssertEquals;
use UnitTester\Asserts\AssertNotNull;
use UnitTester\Asserts\AssertContains;
use UnitTester\Asserts\AssertNotEquals;
use UnitTester\Asserts\AssertContainsKey;
use UnitTester\Exceptions\AssertException;

class AssertableTest implements Assertable {

    /** @var Assertion[] */
    private $assertions = [];
    private $assertionsRan = 0;

    public function getAssertions() 
    {
        return $this->assertions;
    }

    public function getAssertionsRan()
    {
        return $this->assertionsRan;
    }

    public function setAssertionsRan(int $assertionsRan)
    {
        $this->assertionsRan = $assertionsRan;
    }

    public function assertContains(array $array, $value)
    {
        $assertion = new AssertContains($array, $value);
        $this->assertions[] = $assertion;

        $this->assertionsRan++;

        if (!$assertion->assert())
        {
            throw new AssertException('Array does not contain value: ' . $value);
        }
    }
    
    public function assertContainsKey(array $array, $key)
    {
        $assertion = new AssertContainsKey($array, $key);
        $this->assertions[] = $assertion;

        $this->assertionsRan++;

        if (!$assertion->assert())
        {
            throw new AssertException('Array does not contain key: ' . $key);
        }
    }

    public function assertCount(array $array, $expectedCount)
    {
        $assertion = new AssertCount($array, $expectedCount);
        $this->assertions[] = $assertion;

        $this->assertionsRan++;

        if (!$assertion->assert())
        {
            throw new AssertException('Array count does not match - Actual: ' . count($array) . ' | Expected: ' . $expectedCount);
        }
    }

    public function assertEquals($actualValue, $expectedValue)
    {
        $assertion = new AssertEquals($actualValue, $expectedValue);
        $this->assertions[] = $assertion;

        $this->assertionsRan++;

        if (!$assertion->assert())
        {
            throw new AssertException('Values do not match - Actual: ' . $actualValue . ' | Expected: ' . $expectedValue);
        }
    }

    public function assertNotEquals($actualValue, $expectedValue)
    {
        $assertion = new AssertNotEquals($actualValue, $expectedValue);
        $this->assertions[] = $assertion;

        $this->assertionsRan++;

        if (!$assertion->assert())
        {
            throw new AssertException('Values should not match - Actual: ' . $actualValue . ' | Expected: ' . $expectedValue);
        }
    }

    public function assertTrue($value)
    {
        $assertion = new AssertTrue($value);
        $this->assertions[] = $assertion;

        $this->assertionsRan++;

        if (!$assertion->assert())
        {
            throw new AssertException('Value was supposed to be true');
        }
    }

    public function assertFalse($value)
    {
        $assertion = new AssertFalse($value);
        $this->assertions[] = $assertion;

        $this->assertionsRan++;

        if (!$assertion->assert())
        {
            throw new AssertException('Value was supposed to be false');
        }
    }

    public function assertNull($value)
    {
        $assertion = new AssertNull($value);
        $this->assertions[] = $assertion;

        $this->assertionsRan++;

        if (!$assertion->assert())
        {
            throw new AssertException('Value was supposed to be null: ' . $value);
        }
    }

    public function assertNotNull($value)
    {
        $assertion = new AssertNotNull($value);
        $this->assertions[] = $assertion;

        $this->assertionsRan++;

        if (!$assertion->assert())
        {
            throw new AssertException('Value was supposed to not be null');
        }
    }

}