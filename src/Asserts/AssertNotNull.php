<?php

namespace UnitTester\Asserts;

use UnitTester\Asserts\Assertion;

class AssertNotNull implements Assertion {

    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function assert()
    {
        return (!is_null($this->value));
    }

}