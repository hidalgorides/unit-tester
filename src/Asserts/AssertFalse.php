<?php

namespace UnitTester\Asserts;

use UnitTester\Asserts\Assertion;

class AssertFalse implements Assertion {

    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function assert()
    {
        return ($this->value === false);
    }

}