<?php

namespace UnitTester\Asserts;

use UnitTester\Asserts\Assertion;

class AssertContains implements Assertion {

    private $array;
    private $value;

    public function __construct(array $array, $value)
    {
        $this->array = $array;
        $this->value = $value;
    }

    public function assert()
    {
        return (in_array($this->value, $this->array));
    }

}