<?php

namespace UnitTester\Asserts;

use UnitTester\Asserts\Assertion;

class AssertEquals implements Assertion {

    private $actualValue;
    private $expectedValue;

    public function __construct($actualValue, $expectedValue)
    {
        $this->actualValue = $actualValue;
        $this->expectedValue = $expectedValue;
    }

    public function assert()
    {
        return ($this->actualValue === $this->expectedValue);
    }

}