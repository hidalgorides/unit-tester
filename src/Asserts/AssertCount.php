<?php

namespace UnitTester\Asserts;

use UnitTester\Asserts\Assertion;

class AssertCount implements Assertion {

    private $array;
    private $expectedCount;

    public function __construct(array $array, $expectedCount)
    {
        $this->array = $array;
        $this->expectedCount = $expectedCount;
    }

    public function assert()
    {
        return (count($this->array) === $this->expectedCount);
    }

}