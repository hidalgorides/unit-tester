<?php

namespace UnitTester\Asserts;

use UnitTester\Asserts\Assertion;

class AssertTrue implements Assertion {

    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function assert()
    {
        return ($this->value === true);
    }

}