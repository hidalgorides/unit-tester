<?php

namespace UnitTester\Asserts;

use UnitTester\Asserts\Assertion;

class AssertContainsKey implements Assertion {

    private $array;
    private $key;

    public function __construct(array $array, $key)
    {
        $this->array = $array;
        $this->key = $key;
    }

    public function assert()
    {
        return (array_key_exists($this->key, $this->array));
    }

}