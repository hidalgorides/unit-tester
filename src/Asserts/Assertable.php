<?php

namespace UnitTester\Asserts;

interface Assertable {

    public function assertContains(array $array, $value);

    public function assertContainsKey(array $array, $key);

    public function assertCount(array $array, $expectedCount);

    public function assertEquals($actualValue, $expectedValue);

    public function assertNotEquals($actualValue, $expectedValue);

    public function assertTrue($value);

    public function assertFalse($value);

    public function assertNull($value);

    public function assertNotNull($value);

}