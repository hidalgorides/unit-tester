<?php

namespace UnitTester\Asserts;

interface Assertion {

    public function assert();

}